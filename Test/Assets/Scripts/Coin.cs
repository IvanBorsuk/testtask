﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [HideInInspector] public int id;
    
    private void OnCollisionEnter(Collision collision)
    {
        this.gameObject.SetActive(false);
        Player.player.BalanceControl();
        Player.player.isClic = true;

    }
}
