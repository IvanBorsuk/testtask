﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player player;
    public Coin coin;
    public Transform spawnZone;
    [HideInInspector] public int Balance = 30;
    [HideInInspector] public bool isClic = true;

    private Coin temp = null;
    
    private void Awake()
    {
        player = this;
        temp = Instantiate(coin, spawnZone.position, Quaternion.identity);
    }

    public void SpawnCoins()
    {
        temp.transform.position = spawnZone.position;
        temp.gameObject.SetActive(true);
        isClic = false;
    }

    public void BalanceControl()
    {
        Balance--;
        
        if(Balance == 0)
        {
            Balance = 30;
        }
        Score.balance.balanceSow.text = Balance.ToString();
    }
}
